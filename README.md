# gitlab-ee-issues-7408-demonstration

Related issue: https://gitlab.com/gitlab-org/gitlab-ee/issues/7408

Examples:

 * Example 1
   * Query: `* path:readme`
   * Result: README.md, readme/.gitkeep, and README_with_double_extension.md.md
   * Expected? Yes, all of them should be displayed
 * Example 2
   * Query: `* path:readme.md`
   * Result: README.md
   * Expected? Yes
 * Example 3
   * Query: `* path:readme*md`
   * Result: README.md and README_with_double_extension.md.md
   * Expected? Yes
 * Example 4
   * Query: `* filename:readme`
   * Result: README.md, readme/.gitkeep, and README_with_double_extension.md.md
   * Expected? No, only README.md and README_with_double_extension.md.md should be displayed
 * Example 5
   * Query: `* filename:readme.md`
   * Result: -
   * Expected? No, README.md should be displayed
 * Example 6
   * Query: `* filename:readme*md`
   * Result: -
   * Expected? No, README.md and README_with_double_extension.md.md should be displayed 
 * Example 7
   * Query: `* filename:readme*ext`
   * Result: README_with_double_extension.md.md
   * Expected: Yes
